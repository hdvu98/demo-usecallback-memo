import React, { useEffect, useState, memo, useCallback } from "react";
import ReactDOM from "react-dom";

const InputComponent = memo(function(props) {
  console.log("rerender",props.name);
  return (
     <div className="form-group">
    <label htmlFor={props.name} className="form-label">{props.title}</label>
    <input
      className="form-input"
      id={props.name}
      name={props.name}
      type={props.type}
      value={props.value}
      onChange={(e)=>{ props.handleChange(e.currentTarget)}}
      placeholder={props.placeholder} 
    />
  </div>
  );
});

function App() {
  const [data, setData] = useState({
    fullName: '',
    age: '',
    address: '',
    city: ''
  });


  const handleChange = useCallback(currentTarget => {
    //e.persist();
    setData(data => {
      const name = currentTarget.name;
      const value = currentTarget.value;
      return ({...data, [name]: value})
    });
  }, []);

  return (
    <ul>
      <InputComponent value={data['fullName']} name="fullName"
        title="Full Name" handleChange={handleChange} />
      <InputComponent  value={data['age']} name="age"
      title="Age" handleChange={handleChange}/>
      <InputComponent  value={data['address']} name="address"
      title="Address" handleChange={handleChange}/>
      <InputComponent   value={data['city']} name="city"
      title="City" handleChange={handleChange}/>
    </ul>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
